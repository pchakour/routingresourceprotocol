package net.floodlightcontroller.rrp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jboss.netty.util.internal.ConcurrentHashMap;
import org.openflow.util.HexString;
import org.slf4j.Logger;

import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.ImmutablePort;
import net.floodlightcontroller.packet.IPv4;

public class Topology {

	protected static IProvider provider;
	protected static Logger log;
	
	/*
	 * Integer = ip address
	 * String = mac address
	 * IOFSwitch = Switch connected with
	 * Short = Port of switch on which host is connected
	 */
	private Map<Integer, Pair<String, Pair<IOFSwitch, Short>>> hosts;
	
	/*
	 * 
	 */
	private Map<String, Pair<Long, Short>> switchsHardware;
	
	/*
	 * 
	 */
	private Map<Long, List<Pair<Long, Short>>> switchsConnection;

	/*
	 * Long = ID Switch
	 * Integer = VLAN 
	 */
	private Map<Long, Short> switchs;
	
	
	public Topology( IProvider mProvider ) {
		
		this.hosts = new ConcurrentHashMap<Integer, Pair<String, Pair<IOFSwitch, Short>>>();
		this.switchsHardware = new ConcurrentHashMap<String, Pair<Long, Short>>();
		this.switchsConnection = new ConcurrentHashMap<Long, List<Pair<Long, Short>>>();
		this.switchs = new ConcurrentHashMap<Long, Short>();
		provider = mProvider;
		log = provider.getLogger();
		
	}
	
	
	/**
	 * 
	 * @param host
	 * @param connectedTo
	 * 
	 * @return
	 * 
	 */
	
	public Topology add( Integer host, byte[] mac, Pair<IOFSwitch,Short> connectedTo ) {
		
		this.hosts.put( host, new Pair<String, Pair<IOFSwitch,Short>>(HexString.toHexString(mac), connectedTo) );
		
		return this;
		
	}
	
	
	
	/**
	 * 
	 * @param sw
	 * 
	 * @return
	 * 
	 */
	
	public Topology add( IOFSwitch sw ) {
		
		this.switchs.put( sw.getId(), (short) ((short)this.switchs.size()+1) );
		
		for ( ImmutablePort port : sw.getPorts() ) 
			this.switchsHardware.put(HexString.toHexString(port.getHardwareAddress()), new Pair<Long, Short>(sw.getId(), port.getPortNumber()));
		
		return this;
		
	}
	
	
	
	/**
	 * 
	 * @param sw
	 * @param connectedTo
	 * 
	 * @return
	 * 
	 */
	
	public Topology add( IOFSwitch sw, Pair<Long,Short> connectedToSwitchByPort, String macConnected ) {
		
		long id = sw.getId();
		List<Pair<Long,Short>> l = this.switchsConnection.get( id );
		if ( l == null )
			l = new ConcurrentList<Pair<Long,Short>>(new ArrayList<Pair<Long, Short>>());
		
		for ( Pair<Long, Short> p : l ) {
			if ( p.equals(connectedToSwitchByPort) ) 
				return this;
		}
		
		l.add(connectedToSwitchByPort);
		
		this.switchsConnection.put(id, l);
		
		// Add reverse
		l = this.switchsConnection.get( connectedToSwitchByPort.getLeft() );
		if ( l == null )
			l = new ConcurrentList<Pair<Long,Short>>(new ArrayList<Pair<Long, Short>>());
		
		Pair<Long, Short> pi = this.switchsHardware.get(macConnected);
		if ( pi == null )
			return this;
		
		Pair<Long, Short> pa = new Pair<Long, Short>(id, pi.getRight());
		for ( Pair<Long, Short> p : l ) {
			if ( p.equals(pa) ) 
				return this;
		}
		
		l.add(pa);
		
		this.switchsConnection.put(connectedToSwitchByPort.getLeft(), l);
		
		return this;
		
	}
	
	/**
	 * 
	 * @param host
	 * 
	 * @return
	 * 
	 */
	
	public Topology remove( Integer host ) {
		
		if ( log.isInfoEnabled() )
			log.info("Host with ip address "+ IPv4.fromIPv4Address(host) +" is remove from topology");
		
		this.hosts.remove(host);
		
		return this;
		
	}
	
	
	
	/**
	 * 
	 * @param hosts
	 * 
	 * @return
	 * 
	 */
	
	public Topology remove ( List<Integer> hosts ) {
		
		for ( Integer host : hosts ) {
			
			if ( log.isInfoEnabled() )
				log.info("Host with ip address "+ IPv4.fromIPv4Address(host) +" is remove from topology");
			
			this.hosts.remove(host);
			
		}
		
		return this;
		
	}
	
	
	/**
	 * 
	 * @param switchId
	 * 
	 * @return
	 * 
	 */
	
	public Topology removeSwitch ( long switchId ) {
		
		this.switchs.remove(switchId);
		this.switchsConnection.remove(switchId);
		
		for ( Entry<String, Pair<Long, Short>> entry : this.switchsHardware.entrySet() ) {
			
			if ( entry.getValue().getLeft() == switchId )
				this.switchsHardware.remove(entry.getKey());
			
		}
		
		for ( Entry<Integer, Pair<String, Pair<IOFSwitch, Short>>> entry : this.hosts.entrySet() ) {
			
			if ( entry.getValue().getRight().getLeft().getId() == switchId )
				this.hosts.remove(entry.getKey());
			
		}
		
		return this;
		
	}
	
	
	
	/**
	 * 
	 * @param sw
	 * @param port
	 * 
	 * @return
	 * 
	 */
	
	public Topology removeHostConnectedTo( IOFSwitch sw, short port ) {
		
		for ( Integer host : this.getHost(sw, port) ) {
			
			if ( log.isInfoEnabled() )
				log.info("Host with ip address "+ IPv4.fromIPv4Address(host) +" is remove from topology");
			
			this.hosts.remove(host);
			
		}
		
		return this;
		
	}
	
	
	
	/**
	 * 
	 * @param sw
	 * @param port
	 * 
	 * @return
	 * 
	 */
	
	public List<Integer> getHost( IOFSwitch sw, short port ) {
		
		List<Integer> hosts = new ArrayList<Integer>();
		
		for ( Entry<Integer, Pair<String, Pair<IOFSwitch, Short>>>  h : this.hosts.entrySet() ) {
			
			Pair<IOFSwitch, Short> v = h.getValue().getRight();
			if ( v.getLeft().equals(sw) && v.getRight().equals(port) ) 
				hosts.add(h.getKey());
				
		}

		return hosts;
		
	}
	
	
	
	/**
	 * 
	 * @return
	 * 
	 */
	
	public Map<Integer, Pair<String, Pair<IOFSwitch, Short>>> getAllHosts() {
		
		return this.hosts;
		
	}
	
	
	
	
	/**
	 * 
	 * @param ipAddress
	 * 
	 * @return
	 * 
	 */
	
	public String getHostHardware( int ipAddress ) {
		
		return this.hosts.get(ipAddress).getLeft();
		
	}
	
	
	
	
	/**
	 * 
	 * @param hardwareAddress
	 * 
	 * @return
	 * 
	 */
	
	public IOFSwitch getSwitchWithHardware( byte[] hardwareAddress ) {
		
		String macAddress = HexString.toHexString(hardwareAddress);
		if ( this.switchsHardware.containsKey(macAddress) )
			return provider.getFloodlightProvider().getSwitch(this.switchsHardware.get(macAddress).getLeft());
		
		return null;
		
	}
	
	
	public Short getSwitchVlan( Long swId ) {
		
		return this.switchs.get(swId);
		
	}
	
	public Map<Long, Short> getVlanMapping() {
		
		return this.switchs;
		
	}
	
	/**
	 * 
	 * @return
	 * 
	 */
	
	public Map<Long, List<Pair<Short, Pair<Character, Long>>>> getSummary() {
		
		Map<Long, List<Pair<Short, Pair<Character, Long>>>> t = new ConcurrentHashMap<Long, List<Pair<Short, Pair<Character, Long>>>>();
		
		for ( Entry<Long, List<Pair<Long, Short>>> entry : this.switchsConnection.entrySet() )	{
	
			List<Pair<Short, Pair<Character, Long>>> l = new ConcurrentList<Pair<Short, Pair<Character, Long>>>(new ArrayList<Pair<Short, Pair<Character, Long>>>());
			for ( Pair<Long, Short> p : entry.getValue() )
				l.add(new Pair<Short, Pair<Character, Long>>(p.getRight(), new Pair<Character, Long>('S', p.getLeft())) );
			
			for ( Entry<Integer, Pair<String, Pair<IOFSwitch, Short>>> h : this.hosts.entrySet() ) {
				if ( h.getValue().getRight().getLeft().getId() == entry.getKey() ) 
					l.add( new Pair<Short, Pair<Character, Long>>(h.getValue().getRight().getRight(), new Pair<Character, Long>( 'H', new Long(h.getKey())) ));
			}	
		
			t.put( entry.getKey(), l);
		
		}
		
		return t;
		
	}
	
	
	
	/**
	 * 
	 */
	
	public String toString() {
		
		String s = "IpAddress          Mac            SwitchId     SwitchPort\n";
		for ( Entry<Integer, Pair<String, Pair<IOFSwitch, Short>>> entry : this.hosts.entrySet() )
			s += IPv4.fromIPv4Address(entry.getKey())+"     "+entry.getValue().getLeft()+"           "+entry.getValue().getRight().getLeft().getId()+"      "+entry.getValue().getRight().getRight()+"\n";
		s += "\n\n";
		
		for ( Entry<Long, List<Pair<Long, Short>>> entry : this.switchsConnection.entrySet() ) {
			
			for ( Pair<Long, Short> p : entry.getValue() )
				s += "Switch "+entry.getKey()+" is connected to Switch "+ p.getLeft() +" by port "+ p.getRight() +" \n";
			
		}
			
		return s;
		
	}

	
}

