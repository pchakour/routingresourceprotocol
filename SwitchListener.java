package net.floodlightcontroller.rrp;

import java.util.List;

import org.slf4j.Logger;
import net.floodlightcontroller.core.IOFSwitch.PortChangeType;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.IOFSwitchListener;
import net.floodlightcontroller.core.ImmutablePort;

public class SwitchListener implements IOFSwitchListener {

	
	protected static IProvider provider;
	protected static Logger log;
	protected static Discover discover;
	
	
	
	public SwitchListener( IProvider mProvider ) {
		
		provider = mProvider;
		log = provider.getLogger();
		discover = provider.getDiscover();
		
	}
	
	@Override
	public void switchAdded(long switchId) {}

	
	@Override
	public void switchRemoved(long switchId) {

		provider.getTopology().removeSwitch(switchId);
		provider.getFlowPusher().deleteAllFlows();
		provider.getRoutingProcess().run();
		
	}
	
	
	@Override
	public void switchChanged(long switchId) {
		
	}

	
	
	@Override
	public void switchActivated(long switchId) {
		
		IOFSwitch sw = provider.getFloodlightProvider().getSwitch(switchId);
		provider.getTopology().add(sw);
		discover.start( sw );
		
	}

	
	
	@Override
	public void switchPortChanged(long switchId, ImmutablePort port,
			PortChangeType type) {

		IOFSwitch sw = provider.getFloodlightProvider().getSwitch(switchId);
		
		if ( log.isInfoEnabled() )
			log.info("Switch "+switchId+" Port "+port.getName()+" changed to "+type.name());
		
		if ( type.name().equals("DOWN") ) {
				
			List<Integer> hosts = provider.getTopology().getHost(sw, port.getPortNumber());
			provider.getTopology().remove(hosts);
			provider.getRIB().remove(hosts);
			
		}else if ( type.name().equals("UP") )
			discover.start(sw);
		
	}

	

}
