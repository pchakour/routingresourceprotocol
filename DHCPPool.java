package net.floodlightcontroller.rrp;

import java.util.ArrayList;
import java.util.List;

import net.floodlightcontroller.packet.IPv4;

public class DHCPPool {
	
	protected List<Pair<Integer, String>> pool;
	
	
	
	public DHCPPool( String ipStart, String ipEnd ) {
		
		this.pool = new ConcurrentList<Pair<Integer, String>>(new ArrayList<Pair<Integer, String>>());
		for ( int i=IPv4.toIPv4Address(Rrp.IP_NETWORK_START)+1; i<IPv4.toIPv4Address(Rrp.IP_NETWORK_END); i++ )
			this.pool.add(new Pair<Integer, String>(i, null));
		
	}
	
	
	/**
	 * 
	 * @param p
	 * 
	 * @return
	 * 
	 */
	
	public boolean contains(Pair<Integer, String> p) {
		
		return this.pool.contains(p);
		
	}
	
	
	/**
	 * 
	 * @param macAddress
	 * 
	 * @return
	 * 
	 */
	
	public Integer getIP( String macAddress ) {
		
		for ( Pair<Integer, String> p : this.pool ) {
			if ( p.getRight() != null && p.getRight().equals(macAddress) ) {
				return p.getLeft();
			}
		}
		
		return null;
		
	}
	
	
	/**
	 * 
	 * @param ipAddress
	 * 
	 * @return
	 */
	
	public boolean isAvailable( Integer ipAddress ) {
		
		for ( Pair<Integer, String> p : this.pool ) {
			if ( p.getLeft() == ipAddress && p.getRight() == null)
				return true;
		}
		
		return false;
		
	}
	
	
	
	/**
	 * 
	 * @param macAddress
	 * 
	 * @return
	 * 
	 */
	
	public Integer lease( String macAddress ) {
		
		Pair<Integer, String> ipOffered = null;
		for ( Pair<Integer, String> p : this.pool ) {
			if ( p.getRight() == null ) {
				ipOffered = p;
				break;
			}
		}
		
		if ( ipOffered == null ) {
			System.out.println("No available ip address");
			return null;
		}
		
		ipOffered.setRight(macAddress);

		return ipOffered.getLeft();
				
	}
	
	
	/**
	 * 
	 * @param macAddress
	 * 
	 */
	
	public void free( String macAddress ) {
		
		for ( Pair<Integer, String> p : this.pool ) {
			if ( p.getRight() != null && p.getRight().equals(macAddress) ) {
				p.setRight(null);
				break;
			}
		}
		
	}
	
	
}
