package net.floodlightcontroller.rrp;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


import org.jboss.netty.util.internal.ConcurrentHashMap;
import org.slf4j.Logger;

import net.floodlightcontroller.packet.IPv4;



public class RIB {

	/*
	 * Integer = Ip address host
	 */
	private static Map<Integer, List<Resource>> resources;
	
	protected static IProvider provider;
	protected static Logger log;
	
	
	
	public RIB( IProvider mProvider ) {
		
		resources = new ConcurrentHashMap<Integer,List<Resource>>();
		provider = mProvider;
		log = provider.getLogger();
		
	}
	
	
	/**
	 * 
	 * @param ipAddress
	 * @param port
	 * @param protocol
	 * 
	 */
	
	public RIB put( int ipAddress, Resource resource ) {

		if ( resources.containsKey(ipAddress) ) {
			
			List<Resource> l = resources.get(ipAddress);
			for ( Resource r : l ) {
				
				if ( resource.equals(r) ) {
					r.lastUpdate = resource.lastUpdate;
					return this;
				}
				
			}
			
			l.add(resource);
			
		}else {
			
			List<Resource> l = new ConcurrentList<Resource>(new ArrayList<Resource>());
			l.add(resource);
			resources.put(ipAddress, l);
			
		}
			
		return this;
		
	}
	

	
	/**
	 * 
	 * @param ipAddress
	 * @param resource
	 * 
	 * @return
	 * 
	 */
	
	public boolean contains( int ipAddress, Resource resource ) {
	
		if ( resources.containsKey(ipAddress) == false ) 
			return false;
		
		
		List<Resource> l = resources.get(ipAddress);
		for ( Resource r : l ) {
			
			if ( resource.equals(r) )
				return true;
			
		}
		
		return false;
		
	}
	
	
	/**
	 * 
	 * @param ipAddress
	 * 
	 * @return
	 * 
	 */
	
	public boolean contains( int ipAddress ) {
			
		return resources.containsKey(ipAddress);
		
	}
	
	
	
	/**
	 * 
	 * @return 
	 * @return
	 * 
	 */
	
	public RIB remove( int ipAddress, Resource resource ) {
		
		if ( resources.containsKey(ipAddress) == false )
			return this;
		
		for ( Resource r : resources.get(ipAddress) ) {
			
			if ( r.equals(resource) ) {
				resources.get(ipAddress).remove(r);
				if ( resources.get(ipAddress).isEmpty() )
					resources.remove(ipAddress);
				return this;
			}
			
		}
		

		return this;
		
	}
	
	
	
	/**
	 * 
	 * @param ipAddress
	 * 
	 * @return
	 * 
	 */
	
	public RIB remove( int ipAddress ) {

		resources.remove(ipAddress);
		
		return this;
		
	}
	
	
	
	/**
	 * 
	 * @param hosts
	 * 
	 * @return
	 * 
	 */
	
	public RIB remove( List<Integer> hosts ) {
		
		for ( Integer host : hosts )
			resources.remove(host);
	
		return this;
		
	}

	
	
	/**
	 * 
	 * @return
	 * 
	 */
	
	public Set<Entry<Integer, List<Resource>>> getEntries() {
		
		return resources.entrySet();

	}
	
	
	
	/**
	 * 
	 * @param index
	 * 
	 * @return
	 * 
	 */
	
	public Entry<Integer, List<Resource>> getEntry( int index ) {
		
		Entry<Integer, List<Resource>> r = null;

		int i = 0;
		Iterator<Entry<Integer, List<Resource>>> entries = resources.entrySet().iterator();
		
		while( i<resources.size() && i!=index ) {
			r = entries.next();
		}

		
		return r;
	}
	
	
	
	public List<Resource> get( int ipAddress ) {
		
		return resources.get(ipAddress);
		
	}
	
	/**
	 * 
	 */
	
	public String toString() {
		
		String s = "IP Adress     Port    Protocol    Age\n";
		
		for( Entry<Integer, List<Resource>> entry : resources.entrySet() ) {
			
			for ( Resource r : entry.getValue() ) {
				
				String ipAdress = IPv4.fromIPv4Address(entry.getKey());
				Timestamp now = new Timestamp(new Date().getTime());
				int age = (int) (now.getTime() - r.lastUpdate.getTime()) / 1000;
				s += ipAdress + "     "+ r.portNumber +"         "+ r.protocol +"          "+ age + "s\n";
				
			}
		
		}
		
		return s;
		
	}
	
	
}
