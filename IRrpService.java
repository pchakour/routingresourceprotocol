package net.floodlightcontroller.rrp;

import java.util.List;

import net.floodlightcontroller.core.module.IFloodlightService;

public interface IRrpService extends IFloodlightService {

	public List<String> getPool();
	
}
