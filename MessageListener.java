package net.floodlightcontroller.rrp;

import org.openflow.protocol.OFMessage;
import org.openflow.protocol.OFPacketIn;
import org.openflow.protocol.OFType;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.IOFMessageListener;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.packet.ARP;
import net.floodlightcontroller.packet.DHCP;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.IPacket;
import net.floodlightcontroller.packet.IPv4;
import net.floodlightcontroller.packet.TCP;


public class MessageListener implements IOFMessageListener {

	
	protected static IProvider provider;
	protected static Discover discover;
	
	
	public MessageListener( IProvider mProvider ) {
		
		provider = mProvider;
		discover = provider.getDiscover();
		
	}
	
	
	
	@Override
	public String getName() {
		
		return Rrp.class.getSimpleName();
		
	}

	
	@Override
	public boolean isCallbackOrderingPrereq(OFType type, String name) {
		return false;
	}

	
	@Override
	public boolean isCallbackOrderingPostreq(OFType type, String name) {
		return false;
	}
	

	@Override
	public Command receive( IOFSwitch sw, OFMessage msg, FloodlightContext cntx ) {
		
		OFPacketIn pi = ((OFPacketIn) msg);
		Ethernet ethernet = IFloodlightProviderService.bcStore.get(cntx, IFloodlightProviderService.CONTEXT_PI_PAYLOAD);
		Command command = Command.CONTINUE;
		
		switch ( ethernet.getEtherType() ) {
		
			case Ethernet.TYPE_ARP :
				ARP arp = (ARP) ethernet.getPayload();
				if( arp.getOpCode() == ARP.OP_REPLY )
					command = discover.processArpReply(arp, sw, pi);
				else
					command = discover.processArpRequest(arp, sw, pi);
			break;
			
			case Ethernet.TYPE_IPv4 :
				IPv4 ip = (IPv4) ethernet.getPayload();
				IPacket transport = ip.getPayload(); 
				IPacket application = transport.getPayload();
				
				if ( (application!=null) && (application instanceof DHCP) ) 
					command = discover.processDHCP((DHCP)application, sw, pi);
				
				if ( ip.getProtocol() == IPv4.PROTOCOL_TCP ) {
					TCP tcp = (TCP) transport;
					if ( tcp.getFlags() == Rrp.TCP_FLAG_SYN_ACK )
						command = discover.processTcpSynAck(tcp, sw, pi);
				}
			break;
			
			default:
				
			break;
				
		}
		
		
		return command;
		
	}
	
	


}
