package net.floodlightcontroller.rrp;

import java.util.ArrayList;
import java.util.List;

import org.openflow.protocol.OFFlowMod;
import org.openflow.protocol.OFMatch;
import org.openflow.protocol.action.OFAction;
import org.openflow.protocol.action.OFActionDataLayerDestination;
import org.openflow.protocol.action.OFActionNetworkLayerDestination;
import org.openflow.protocol.action.OFActionOutput;
import org.openflow.protocol.action.OFActionType;

import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.IPv4;

import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;

import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.util.AppCookie;

public class FlowPusher {

	private Topology topology;
	private long cookie;
	protected IStaticFlowEntryPusherService flowPusher;
	
	
	private static final short APP_ID = 40;
    static {
        AppCookie.registerApp(APP_ID, "RoutingResourceProtocol");
    }
    
	public FlowPusher(IProvider provider, IStaticFlowEntryPusherService sf ) {
		
		this.flowPusher = sf;
		this.topology = provider.getTopology();
		this.cookie = AppCookie.makeCookie(APP_ID, 0);
		
	}
	
	
	/**
	 * 
	 * @param name
	 * @param sw
	 * @param port
	 * @param ipAddress
	 * @param priority
	 */
	public void push( String name, IOFSwitch sw, Pair<Long, Short> nextHop, int ipAddress, short priority ) {
 
		OFMatch match = new OFMatch();
		match.setDataLayerType(Ethernet.TYPE_IPv4); 
		match.setNetworkDestination(ipAddress);
//		match.setDataLayerVirtualLan(topology.getSwitchVlan(sw.getId()));
		
//		OFActionVirtualLanIdentifier vl = new OFActionVirtualLanIdentifier();
//		vl.setVirtualLanIdentifier(this.topology.getSwitchVlan(nextHop.getLeft()));
//		vl.setType(OFActionType.SET_VLAN_ID);
//		vl.setLength((short) OFActionVirtualLanIdentifier.MINIMUM_LENGTH);
		
		List<OFAction> actions = new ArrayList<OFAction>();
//		actions.add(vl);
		actions.add(new OFActionOutput( nextHop.getRight()) ); 
		
		this._push(name, sw, priority, match, actions, OFFlowMod.MINIMUM_LENGTH + OFActionOutput.MINIMUM_LENGTH /*+ vl.getLengthU()*/);
		
	}
	
	
	/**
	 * 
	 * @param name
	 * @param sw
	 * @param port
	 * @param ipAddress
	 * @param resource
	 * @param priority
	 * 
	 */
	
	public void push( String name, IOFSwitch sw, Pair<Long, Short> nextHop, Long ipAddress, Resource resource, short priority ) {
	
		OFMatch match = new OFMatch();
		match.setDataLayerType(Ethernet.TYPE_IPv4); 
		match.setNetworkProtocol(IPv4.PROTOCOL_TCP);
		match.setTransportDestination(resource.portNumber);
		
		OFActionNetworkLayerDestination nd = new OFActionNetworkLayerDestination(ipAddress.intValue());
		nd.setNetworkAddress(ipAddress.intValue());
		nd.setType(OFActionType.SET_NW_DST);
		nd.setLength((short)OFActionNetworkLayerDestination.MINIMUM_LENGTH);
		
		OFActionDataLayerDestination dl = new OFActionDataLayerDestination();
		dl.setDataLayerAddress(Ethernet.toMACAddress(this.topology.getHostHardware(ipAddress.intValue())));
		dl.setType(OFActionType.SET_DL_DST);
		dl.setLength((short)OFActionDataLayerDestination.MINIMUM_LENGTH);
		
		OFActionOutput po = new OFActionOutput();
		po.setPort(nextHop.getRight());
		po.setType(OFActionType.OUTPUT);
		po.setLength((short) OFActionOutput.MINIMUM_LENGTH);
		
		List<OFAction> actions = new ArrayList<OFAction>(); 
		actions.add(nd);
		actions.add(dl);
		actions.add(po);
		
		this._push(name, sw, priority, match, actions, 
				OFFlowMod.MINIMUM_LENGTH + po.getLengthU() +
				nd.getLengthU() + dl.getLengthU() );
		
	}
	
	
	public void push(String name, IOFSwitch sw, short vlan, short priority) {

		OFMatch match = new OFMatch();
		match.setDataLayerVirtualLan(vlan);
		
		List<OFAction> actions = new ArrayList<OFAction>();
		
		this._push(name, sw, priority, match, actions, OFFlowMod.MINIMUM_LENGTH);
		
	}

	
	private void _push( String name, IOFSwitch sw, short priority, OFMatch match, List<OFAction> actions, int length ) {
		
		OFFlowMod flowMod = new OFFlowMod();   
		flowMod.setMatch( match );
		flowMod.setActions( actions );
		flowMod.setCookie( this.cookie );
		flowMod.setBufferId( -1 );     
		flowMod.setPriority( priority );
		flowMod.setCommand( OFFlowMod.OFPFC_ADD );
		flowMod.setIdleTimeout( (short) 10 );
		flowMod.setHardTimeout( (short) 10 );
		flowMod.setFlags( OFFlowMod.OFPFF_SEND_FLOW_REM ); 
		flowMod.setLengthU(length);
		
		flowPusher.addFlow(name, flowMod, sw.getStringId());

	}
	
	public void deleteFlowsForSwitch( long dpid ) {
		
		this.flowPusher.deleteFlowsForSwitch(dpid);
		
	}
	
	public void deleteAllFlows() {
		
		this.flowPusher.deleteAllFlows();
		
	}

	
}

