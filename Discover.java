package net.floodlightcontroller.rrp;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.ImmutablePort;
import net.floodlightcontroller.core.IListener.Command;
import net.floodlightcontroller.packet.ARP;
import net.floodlightcontroller.packet.BasePacket;
import net.floodlightcontroller.packet.DHCP;
import net.floodlightcontroller.packet.DHCP.DHCPOptionCode;
import net.floodlightcontroller.packet.DHCPOption;
import net.floodlightcontroller.packet.DHCPPacketType;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.IPv4;
import net.floodlightcontroller.packet.TCP;
import net.floodlightcontroller.packet.UDP;

import org.openflow.protocol.OFMatch;
import org.openflow.protocol.OFPacketIn;
import org.openflow.protocol.OFPacketOut;
import org.openflow.protocol.OFType;
import org.openflow.protocol.action.OFAction;
import org.openflow.protocol.action.OFActionOutput;
import org.openflow.util.HexString;
import org.slf4j.Logger;


public class Discover {

	protected static IProvider provider;
	protected static Logger log;
	protected Topology topology;
	protected static DHCPPool pool;
	
	
	
	public Discover( IProvider mProvider ) {
		
		provider = mProvider;
		log = provider.getLogger();
		topology = provider.getTopology();
		pool = new DHCPPool(Rrp.IP_NETWORK_START, Rrp.IP_NETWORK_END);
		
	}
	

	
	/**
	 * 
	 * @param sw
	 * @param ipNetworkStart
	 * @param ipNetworkEnd
	 * 
	 */
	
	public void start ( IOFSwitch sw ) {
				
		System.out.println("START DISCOVER ON SWITCH "+sw.getId());
		for ( ImmutablePort outputPort : sw.getEnabledPorts() ) {
			if ( outputPort.getPortNumber() >= 0 ) {
				System.out.println("ENABLE PORT "+outputPort.getPortNumber()+" ON SW "+sw.getId());
				this._send(_generateArpRequest(sw, outputPort, Rrp.IP_CONTROLLER), sw, outputPort, null);
			}
		}
				
	}
	
	
	
	
	/**
	 * 
	 * @param sw
	 * @param outputPort
	 * @param ipDst
	 * 
	 * @return
	 * 
	 */
	
	private Ethernet _generateArpRequest ( IOFSwitch sw, ImmutablePort outputPort, String ipDst ) {
		
		ARP arpRequest = new ARP()
						.setHardwareType(ARP.HW_TYPE_ETHERNET)
				        .setProtocolType(ARP.PROTO_TYPE_IP)
				        .setHardwareAddressLength((byte) 6)
				        .setProtocolAddressLength((byte) 4)
				        .setOpCode(ARP.OP_REQUEST)
				        .setSenderHardwareAddress(outputPort.getHardwareAddress())
				        .setSenderProtocolAddress(IPv4.toIPv4AddressBytes(Rrp.IP_CONTROLLER))
				        .setTargetHardwareAddress(Ethernet.toMACAddress("FF:FF:FF:FF:FF:FF"))
				        .setTargetProtocolAddress(IPv4.toIPv4AddressBytes(ipDst));
		
		Ethernet ethernet = new Ethernet()	
							.setDestinationMACAddress("FF:FF:FF:FF:FF:FF")
							.setSourceMACAddress(outputPort.getHardwareAddress())
							.setEtherType(Ethernet.TYPE_ARP);
		
		ethernet.setPayload(arpRequest);
							
		return ethernet;
		
	}
	
	
	
	
	
	/**
	 * 
	 * @param arpRequest
	 * @param sw
	 * @param outputPort
	 * 
	 * @return
	 * 
	 */
	
	private Ethernet _generateArpReply ( ARP arpRequest, IOFSwitch sw, ImmutablePort outputPort ) {
		
		ARP arpReply = new ARP()
						.setHardwareType(ARP.HW_TYPE_ETHERNET)
				        .setProtocolType(ARP.PROTO_TYPE_IP)
				        .setHardwareAddressLength((byte) 6)
				        .setProtocolAddressLength((byte) 4)
				        .setOpCode(ARP.OP_REPLY)
				        .setSenderHardwareAddress(outputPort.getHardwareAddress())
				        .setSenderProtocolAddress(IPv4.toIPv4AddressBytes(Rrp.IP_CONTROLLER))
				        .setTargetHardwareAddress(arpRequest.getSenderHardwareAddress())
				        .setTargetProtocolAddress(arpRequest.getSenderProtocolAddress());
		
		Ethernet ethernet = new Ethernet()	
							.setDestinationMACAddress(arpRequest.getSenderHardwareAddress())
							.setSourceMACAddress(outputPort.getHardwareAddress())
							.setEtherType(Ethernet.TYPE_ARP);
		
		ethernet.setPayload(arpReply);
							
		return ethernet;
		
	}


	
	
	
	/**
	 * 
	 * @param data
	 * @param sw
	 * @param outputPort
	 * @param cntx
	 * 
	 * @return
	 * 
	 */
	
	private boolean _send ( BasePacket data , IOFSwitch sw, ImmutablePort outputPort , FloodlightContext cntx) {
		
		OFPacketOut po = (OFPacketOut) provider.getFloodlightProvider().getOFMessageFactory().getMessage(OFType.PACKET_OUT);
        List<OFAction> actions = new ArrayList<OFAction>();
        actions.add(new OFActionOutput(outputPort.getPortNumber(), (short)0xFFFF));
        po.setActions(actions);
        po.setActionsLength((short) OFActionOutput.MINIMUM_LENGTH);
        short poLength = (short)(po.getActionsLength() + OFPacketOut.MINIMUM_LENGTH);
        po.setBufferId(OFPacketOut.BUFFER_ID_NONE);  
        
        byte[] d = data.serialize();
        
        poLength += d.length;
        po.setLength(poLength);
        po.setPacketData(d);
        
        try {
        	
			sw.write(po, cntx);
			
			return true;
			
		} catch (IOException e) {
			
			if( log.isErrorEnabled() )
				log.error(e.getMessage());
			
			return false;
		}
        
	}
	
	
	
	
	/**
	 * 
	 * @param arpReply
	 * @param sw
	 * @param pi
	 * 
	 * @return
	 * 
	 */
	
	public Command processArpReply ( ARP arpReply , IOFSwitch sw, OFPacketIn pi ) {
		
		if ( IPv4.toIPv4Address(arpReply.getTargetProtocolAddress()) != IPv4.toIPv4Address(Rrp.IP_CONTROLLER) )
			return Command.CONTINUE;
				
		Pair<IOFSwitch, Short> i = new Pair<IOFSwitch, Short>(sw, pi.getInPort());
		provider.getTopology().add( IPv4.toIPv4Address(arpReply.getSenderProtocolAddress()), arpReply.getSenderHardwareAddress(), i);
		
		if ( log.isInfoEnabled() )
			log.info("Host with ip address "+ IPv4.fromIPv4Address(IPv4.toIPv4Address(arpReply.getSenderProtocolAddress()))+" is detected");
		
		this.scanPort( IPv4.toIPv4Address(arpReply.getTargetProtocolAddress()), IPv4.toIPv4Address(arpReply.getSenderProtocolAddress()), 
				arpReply.getTargetHardwareAddress(),arpReply.getSenderHardwareAddress(), sw, pi.getInPort() );
	
		return Command.STOP;
		
	}
	
	
	
	
	
	/**
	 * 
	 * @param match
	 * @param sw
	 * @param outputPort
	 * 
	 */
	
	public void scanPort( int networkSrc, int networkDst, byte[] datalayerSrc, byte[] datalayerDst
					, IOFSwitch sw, Short outputPort ) {
		
		for ( short portDst=1 ; portDst<1000; portDst++ ) {
			
			this._sendTcp( networkSrc, networkDst, datalayerSrc, datalayerDst, 0, sw, outputPort, portDst, Rrp.TCP_FLAG_SYN);
			
		}
		
	}
	
	
	
	
	
	/**
	 * 
	 * @param DHCP dhcp
	 * @param sw
	 * @param pi
	 * 
	 * @return
	 * 
	 */
	
	public Command processDHCP( DHCP dhcp, IOFSwitch sw, OFPacketIn pi ) {
		
		OFMatch match = new OFMatch();
		match.loadFromPacket(pi.getPacketData(), pi.getInPort());
		
		Command command = Command.CONTINUE;
		
		switch ( dhcp.getPacketType() ) {
			
			case DHCPDISCOVER:
				
				Integer ipOffered = pool.lease(HexString.toHexString(match.getDataLayerSource()));
				System.out.println("IP address offered "+IPv4.fromIPv4Address(ipOffered));
				command = this._sendDHCPReply("OFFER", dhcp, ipOffered, sw, pi);
				
			break;
			
			case DHCPREQUEST:
				
				DHCPOption opt = dhcp.getOption(DHCPOptionCode.OptionCode_RequestedIP);
				int ipAddress = IPv4.toIPv4Address(opt.getData());
				String macAddress = HexString.toHexString(match.getDataLayerSource());
				
				if (  pool.isAvailable(ipAddress) ) {
					pool.free(macAddress);
					pool.lease(macAddress);
				}
				
				Integer ip = pool.getIP(macAddress);
				if ( ip!=null && ip.equals(ipAddress) ) {
					
					if ( log.isInfoEnabled() )
						log.info("Host with address ip "+IPv4.fromIPv4Address(ipAddress)+" on the network");
					
					command = this._sendDHCPReply("ACK", dhcp, ipAddress, sw, pi);
					topology.add(ipAddress, match.getDataLayerSource(), new Pair<IOFSwitch, Short>(sw, pi.getInPort()));
					this.scanPort( IPv4.toIPv4Address(Rrp.IP_CONTROLLER), ipAddress, 
							match.getDataLayerDestination(), match.getDataLayerSource(), sw, pi.getInPort());
					
				} else {
					if ( log.isInfoEnabled() )
						log.info("Free ip address of mac "+ macAddress);
					pool.free(macAddress);
				}
			
				
				command = Command.STOP;
			break;
			
			default:
			break;
		
		}
		
		return command;
		
	}
	
	
	
	
	
	/**
	 * 
	 * @param dhcp
	 * @param sw
	 * @param pi
	 * 
	 * @return
	 * 
	 */
	
	private Command _sendDHCPReply( String type, DHCP dhcpRequest, Integer ipOffered, IOFSwitch sw, OFPacketIn pi ) {
				
		OFMatch match = new OFMatch();
		match.loadFromPacket(pi.getPacketData(), pi.getInPort());
		
		DHCPOption subnet = new DHCPOption();
		subnet.setCode(DHCPOptionCode.OptionCode_SubnetMask.getValue());
		subnet.setLength((byte)4);
		subnet.setData(IPv4.toIPv4AddressBytes("255.255.255.0"));
		
		DHCPOption dhcpReply = new DHCPOption();
		dhcpReply.setCode(DHCPOptionCode.OptionCode_MessageType.getValue());
		dhcpReply.setLength((byte)1);
		
		if ( type.equals("OFFER") )
			dhcpReply.setData(BigInteger.valueOf(DHCPPacketType.DHCPOFFER.getValue()).toByteArray());
		else if ( type.equals("ACK") )
			dhcpReply.setData(BigInteger.valueOf(DHCPPacketType.DHCPACK.getValue()).toByteArray());
		
		DHCPOption time = new DHCPOption();
		time.setCode(DHCPOptionCode.OptionCode_LeaseTime.getValue());
		time.setLength((byte)4);
		time.setData(BigInteger.valueOf(86400).toByteArray());
		
		DHCPOption router = new DHCPOption();
		router.setCode((byte)3);
		router.setLength((byte) 4);
		router.setData(IPv4.toIPv4AddressBytes(Rrp.IP_CONTROLLER));
		
		DHCPOption end = new DHCPOption();
		end.setCode(DHCPOptionCode.OptionCode_END.getValue());
		end.setLength((byte)0);
		
		DHCPOption pad = new DHCPOption();
		pad.setCode((byte)0);
		pad.setLength((byte)0);
		
		List<DHCPOption> options = new ArrayList<DHCPOption>();
		options.add(dhcpReply);
		options.add(router);
		options.add(subnet);
		options.add(time);
		options.add(end);
		options.add(end);
		options.add(pad);
		
		DHCP offer = new DHCP();
		offer.setGatewayIPAddress(IPv4.toIPv4Address(Rrp.IP_CONTROLLER));
		offer.setClientHardwareAddress(dhcpRequest.getClientHardwareAddress());
		offer.setClientIPAddress(dhcpRequest.getClientIPAddress());
		offer.setServerIPAddress(IPv4.toIPv4Address(Rrp.IP_CONTROLLER));
		offer.setHardwareType(DHCP.HWTYPE_ETHERNET);
		offer.setOpCode(DHCP.OPCODE_REPLY);
		offer.setServerIPAddress(IPv4.toIPv4Address(Rrp.IP_CONTROLLER));
		offer.setYourIPAddress(ipOffered);
		offer.setTransactionId(dhcpRequest.getTransactionId());
		offer.setHardwareAddressLength(dhcpRequest.getHardwareAddressLength());
		offer.setOptions(options);
		
		UDP udp = new UDP();
        udp.setDestinationPort((short) 68);
        udp.setSourcePort((short) 67);
        udp.setPayload(offer);
        
        IPv4 ip = new IPv4();
        ip.setVersion((byte) 4);
        ip.setDestinationAddress(IPv4.toIPv4Address("255.255.255.255"));
        ip.setSourceAddress(IPv4.toIPv4Address(Rrp.IP_CONTROLLER));
        ip.setFlags(IPv4.IPV4_FLAGS_DONTFRAG);
        ip.setProtocol(IPv4.PROTOCOL_UDP);
        ip.setFragment(false);
        ip.setIdentification((short)0);
        ip.setTtl((byte) 64);
        ip.setPayload(udp);
     
        Ethernet ethernet = new Ethernet().setSourceMACAddress(match.getDataLayerSource())
                .setDestinationMACAddress(match.getDataLayerDestination())
                .setEtherType(Ethernet.TYPE_IPv4);
        ethernet.setPayload(ip);
        
        this._send(ethernet, sw, sw.getPort(pi.getInPort()), null);
        
        return Command.STOP;
        
	}
	
	
	
	
	/**
	 * 
	 * @param arpRequest
	 * @param sw
	 * @param pi
	 * 
	 * @return
	 * 
	 */
	
	public Command processArpRequest( ARP arpRequest, IOFSwitch sw, OFPacketIn pi ) {
		
		if ( IPv4.toIPv4Address(arpRequest.getSenderProtocolAddress()) != IPv4.toIPv4Address(Rrp.IP_CONTROLLER) &&  
				 IPv4.toIPv4Address(arpRequest.getTargetProtocolAddress()) != IPv4.toIPv4Address(Rrp.IP_CONTROLLER))
			return Command.CONTINUE;
		
		if ( IPv4.toIPv4Address(arpRequest.getSenderProtocolAddress()) == IPv4.toIPv4Address(Rrp.IP_CONTROLLER) ) {
			
			
			
			if ( provider.getLogger().isInfoEnabled() )
				provider.getLogger().info("Switch connection is added in topology (sw"+sw.getId()+")");
			
			IOFSwitch s = topology.getSwitchWithHardware(arpRequest.getSenderHardwareAddress());
			if ( s != null )
				topology.add(sw, new Pair<Long, Short>(s.getId(), pi.getInPort()), HexString.toHexString(arpRequest.getSenderHardwareAddress()));
		}
		
		if (  IPv4.toIPv4Address(arpRequest.getSenderProtocolAddress()) != IPv4.toIPv4Address(Rrp.IP_CONTROLLER) &&
				IPv4.toIPv4Address(arpRequest.getTargetProtocolAddress()) == IPv4.toIPv4Address(Rrp.IP_CONTROLLER) ) {

			ImmutablePort output = sw.getPort(pi.getInPort());
			this._send(_generateArpReply(arpRequest, sw, output), sw, output, null);
			
		}
		
		return Command.STOP;
		
	}

	
	
	
	
	
	/**
	 * 
	 * @param tcp
	 * @param sw
	 * @param inPort
	 * 
	 */
	
	public Command processTcpSynAck(TCP tcp, IOFSwitch sw, OFPacketIn pi ) {
		
		OFMatch match = new OFMatch();
		match.loadFromPacket(pi.getPacketData(), pi.getInPort());
		if ( match.getNetworkDestination() != IPv4.toIPv4Address(Rrp.IP_CONTROLLER) )
			return Command.CONTINUE;
		
		provider.getRIB().put(match.getNetworkSource(), new Resource(tcp.getSourcePort(), Resource.PROTOCOL_TCP));
		
		
		
		if ( log.isInfoEnabled() )
			log.info("Port "+tcp.getSourcePort()+"/tcp is open on host "+IPv4.fromIPv4Address(match.getNetworkSource()));
		
		//Send a tcp RST
		_sendTcp ( match.getNetworkSource(), match.getNetworkDestination(), match.getDataLayerSource(), match.getDataLayerDestination(), 
					tcp.getAcknowledge(), sw, pi.getInPort(), tcp.getSourcePort(), Rrp.TCP_FLAG_RST);
		
		return Command.STOP;
		
	}

	
	
	
	

	/**
	 * 
	 * @param match
	 * @param seq
	 * @param sw
	 * @param port
	 * 
	 */
	
	private void _sendTcp ( int networkSrc, int networkDest, byte[] datalayerSrc, byte[] datalayerDst, 
						int seq, IOFSwitch sw, short outputPort, short portDst, short flags ) {
		
		TCP tcp = new TCP();
        tcp.setDestinationPort((short) portDst);
        tcp.setSourcePort((short) 35796);
        tcp.setWindowSize((short) 1024);
        tcp.setFlags(flags);
        tcp.setSequence(seq);
        
        IPv4 ip = new IPv4();
        ip.setVersion((byte) 4);
        ip.setDestinationAddress(networkDest);
        ip.setSourceAddress(networkSrc);
        ip.setFlags(IPv4.IPV4_FLAGS_DONTFRAG);
        ip.setProtocol(IPv4.PROTOCOL_TCP);
        ip.setFragment(false);
        ip.setIdentification((short)0);
        ip.setTtl((byte) 64);
        ip.setPayload(tcp);
     
        Ethernet ethernet = new Ethernet().setSourceMACAddress(datalayerSrc)
                .setDestinationMACAddress(datalayerDst)
                .setEtherType(Ethernet.TYPE_IPv4);
        ethernet.setPayload(ip);
        
        this._send(ethernet, sw, sw.getPort(outputPort), null);
        
	}	
	
}
