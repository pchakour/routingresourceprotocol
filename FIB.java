package net.floodlightcontroller.rrp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.floodlightcontroller.packet.IPv4;

import org.jboss.netty.util.internal.ConcurrentHashMap;

public class FIB {

	/*
	 * Long = Id of Switch
	 * Integer = IP address of host
	 * Pair
	 * 	Long = next switch
	 * 	Short = output port
	 * 	
	 */
	private Map<Long, Map<Long, List<Pair<Long,Short>>>> routes;
	
	public FIB( IProvider provider ) {
		
		this.routes = new ConcurrentHashMap<Long, Map<Long, List<Pair<Long,Short>>>>();
		
	}
	
	
	public void add( long idSw, int level, long nextHop, short pOut, long ipAddress ) {

		Map<Long, List<Pair<Long,Short>>> ro = this.routes.get(idSw);
		if ( ro == null )
			this.routes.put( idSw, new ConcurrentHashMap<Long, List<Pair<Long,Short>>>());

		List<Pair<Long,Short>> listPo = this.routes.get(idSw).get(ipAddress);
		if ( listPo == null ) {
			listPo = new ArrayList<Pair<Long,Short>>();
			listPo.add(new Pair<Long, Short>(nextHop, pOut));
		}else {
			if ( level >= listPo.size() )
				listPo.add(new Pair<Long, Short>(nextHop, pOut));
			else
				listPo.add(level, new Pair<Long, Short>(nextHop, pOut));
		}
		
		this.routes.get(idSw).put( ipAddress, listPo );
		
	}
	

	public Map<Long, List<Pair<Long,Short>>> get( Long swId ) {
		
		return this.routes.get(swId);
		
	}

	
	public void flush() {
		
		this.routes = new ConcurrentHashMap<Long, Map<Long, List<Pair<Long,Short>>>>();
		
	}
	
	
	@Override
	public String toString() {
		
		String s = "------------ FIB -----------\n";
		
		for ( Entry<Long, Map<Long, List<Pair<Long,Short>>>>  entry : this.routes.entrySet() ) {
			s += "--- Reachable host from switch "+entry.getKey()+" ---\n";
			for ( Entry<Long, List<Pair<Long,Short>>> host : entry.getValue().entrySet() )
				s += IPv4.fromIPv4Address(host.getKey().intValue()) + " output port "+host.getValue()+"\n";
		}
		
		return s;
		
	}
	
}
