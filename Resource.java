package net.floodlightcontroller.rrp;

import java.sql.Timestamp;
import java.util.Date;

public class Resource {

	public final static String PROTOCOL_TCP = "TCP";
	public final static String PROTOCOL_UDP = "UDP";
	
	public short portNumber;
	public String protocol;
	public Timestamp lastUpdate;
	
	
	
	public Resource( short portNumber, String protocol ) {
		
		this.portNumber = portNumber;
		this.protocol = protocol;
		this.lastUpdate = new Timestamp(new Date().getTime());
		
	}
	
	
	
	public Resource( short portNumber, String protocol, Timestamp lastUpdate ) {
		
		this.portNumber = portNumber;
		this.protocol = protocol;
		this.lastUpdate = lastUpdate;
		
	}
	
	
	
	/**
	 * 
	 * @param r
	 * 
	 * @return
	 * 
	 */
	
	public boolean equals( Resource r ) {
	
		return this.portNumber == r.portNumber && this.protocol == r.protocol;
		
	}
	
}
