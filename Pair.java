package net.floodlightcontroller.rrp;

public class Pair<L,R> {

	  private  L left;
	  private  R right;

	  
	  public Pair(L left, R right) {
	    this.left = left;
	    this.right = right;
	  }

	  public L getLeft() { return left; }
	  public R getRight() { return right; }
	  
	  
	  /**
	   * 
	   * @param left
	   * 
	   */
	  
	  public void setLeft( L left ) {
		  
		  this.left = left;
		  
	  }
	  
	  
	  /**
	   * 
	   * @param right
	   * 
	   */
	  
	  public void setRight( R right ) {
		  
		  this.right = right; 
		  
	  }

	  @Override
	  public int hashCode() { return left.hashCode() ^ right.hashCode(); }

	  @Override
	  public boolean equals(Object o) {
		  
	    if (o == null) return false;
	    if (!(o instanceof Pair)) return false;
	    Pair<?,?> pairo = (Pair<?,?>) o;
	    
	    return this.left.equals(pairo.getLeft()) &&
	           this.right.equals(pairo.getRight());
	    
	  }
	  
	  
	  
	  /**
	   * 
	   * @param p
	   * 
	   * @return
	   * 
	   */
	  
	  public boolean equals( Pair<L,R> p ){
		 
		return p.getLeft().equals(this.left) && p.getRight().equals(this.right);
		  
	  }

	}