package net.floodlightcontroller.rrp;

import net.floodlightcontroller.core.IFloodlightProviderService;

import org.slf4j.Logger;

public interface IProvider {

	public IFloodlightProviderService getFloodlightProvider();
	public Logger getLogger();
	public MessageListener getMessageListener();
	public SwitchListener getSwitchLitener();
	public Topology getTopology();
	public RIB getRIB();
	public FIB getFIB();
	public Discover getDiscover();
	public GarbageCollector getGarbageCollector();
	public UpdateManager getUpdateManager();
	public FlowPusher getFlowPusher();
	public RoutingProcess getRoutingProcess();
	
}
