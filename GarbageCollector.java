package net.floodlightcontroller.rrp;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;

import net.floodlightcontroller.packet.IPv4;


public class GarbageCollector extends Thread {

	protected static IProvider provider;
	private final static int maxAge = 30; 
	private static Logger log;
	private static RIB table;
	
	
	
	public GarbageCollector ( IProvider mProvider ) {

		provider = mProvider;
		log = provider.getLogger();
		table = provider.getRIB();
		
	}
	
	
	
	/**
	 * 
	 */
	
	public void run() {
		
		
		for ( Entry<Integer, List<Resource>> entry : table.getEntries() ) {
			
			for ( Resource r : entry.getValue() ) {
				
				Timestamp now = new Timestamp(new Date().getTime());
				if ( now.getTime() - r.lastUpdate.getTime() > maxAge * 1000) {
					
					int host = entry.getKey();
					
					if ( log.isInfoEnabled() )
						log.info("The open port "+r.portNumber+" on the host "+IPv4.fromIPv4Address(host)+" is out of age");
					
					
					table.remove(host, r);
					
				}
				
			}
			
		}
		
	}
	
}
