package net.floodlightcontroller.rrp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.floodlightcontroller.core.IOFSwitch;

public class RoutingProcess implements Runnable {

	protected static IProvider provider;
	protected FlowPusher flowPusher;
	private Topology topology;
	private Map<Long, List<Pair<Short, Pair<Character, Long>>>> topologySummary;
	private FIB fib;
	private RIB rib;
	
	/**
	 *  Constructor
	 */
	
	public RoutingProcess( IProvider mProvider ) {
		
		provider = mProvider;
		this.flowPusher = provider.getFlowPusher();
		this.topology = provider.getTopology();	
		this.fib = provider.getFIB();
		this.rib = provider.getRIB();
		
	}
	
	
	/**
	 *  init routing process for all switch
	 */
	
	public void initScan (){
		
		this.fib.flush();
		
		this.topologySummary = topology.getSummary();
		displaySummary();
		
		for ( Entry<Long, List<Pair<Short, Pair<Character, Long>>>> s : this.topologySummary.entrySet() ) {
		
			if ( provider.getFloodlightProvider().getSwitch(s.getKey()) == null )
				continue;
			
			this.flowPusher.deleteFlowsForSwitch(s.getKey());
			if (provider.getLogger().isInfoEnabled()) {
				provider.getLogger().info("Flows on switch "+s.getKey()+" are removed");
				provider.getLogger().info("Make switch tree for switch "+s.getKey());
			}
			
			List<Long> l = new ArrayList<Long>();
			scanLocalPort(s.getKey(), s.getKey(), 0, (short) 0, l);
			
		}
		
	}
	
	
	/**
	 *  Scan The Given switch
	 */
	
	public void scanLocalPort( long sw, long swSource, int level , short po, List<Long> list ){
				
		System.out.println("("+sw+", "+swSource+", "+level+", "+po+", "+list+")");
		if ( list.contains(sw) )
			return;
			
		List<Pair<Short, Pair<Character, Long>>> s = this.topologySummary.get(sw);
		if ( s == null )
			return;
		
		for ( Pair<Short, Pair<Character, Long>> p : s ) {
			
			if (level == 0) 
				po = p.getLeft();
			
			if ( p.getRight().getLeft().equals('H') ) {
				
				Long nextHop = null;
				if ( list.size() > 1 )
					nextHop = list.get(1);
				
				try {
					this.fib.add(swSource, level, nextHop, po, p.getRight().getRight());
				}catch(Exception e){}
				
			}else {
				list.add(sw);
				scanLocalPort(p.getRight().getRight(), swSource, level+1, po, list);
			}
			
		}
		
	}
	
	
	public void run() {
		System.out.println("Routing process is running");
		initScan();
		FlowPusher flowPusher = provider.getFlowPusher();
		for ( Entry<Long, IOFSwitch> sw : provider.getFloodlightProvider().getAllSwitchMap().entrySet() ) {
			
			Map<Long, List<Pair<Long,Short>>> fibSw = this.fib.get(sw.getKey());
			System.out.println("FIB SW"+sw.getKey()+" "+fibSw);
			if ( fibSw == null )
				continue;
			
			for ( Entry<Long, List<Pair<Long,Short>>> host : fibSw.entrySet() ) {
				
				int i=host.getValue().size()*10;
				for ( Pair<Long, Short> nextHop : host.getValue() ) {
					flowPusher.push(nextHop.getRight()+""+host.getKey()+""+i, sw.getValue(), nextHop, host.getKey().intValue(), (short)i);
					List<Resource> l = this.rib.get(host.getKey().intValue());
					if ( l == null )
						continue;
					
					for ( Resource resource :  l )
						flowPusher.push(resource.portNumber+""+host.getKey()+""+(i+5), sw.getValue(), nextHop, host.getKey(), resource, (short)(i+5));
				}
				i -= 10;
			}
		}
		
		
	}
	
	public void displaySummary() {
		
		for ( Entry<Long, List<Pair<Short, Pair<Character, Long>>>> entry : this.topologySummary.entrySet() ) {
			
			System.out.println("--------Switch "+entry.getKey()+" -------");
			for ( Pair<Short, Pair<Character, Long>> p : entry.getValue() )
				System.out.println("On port "+p.getLeft()+" type="+p.getRight().getLeft()+" address="+p.getRight().getRight());
			
		}
		
	}
	
	
	
}
