package net.floodlightcontroller.rrp.restlet;

import org.restlet.Context;
import org.restlet.Restlet;
import org.restlet.routing.Router;

import net.floodlightcontroller.restserver.RestletRoutable;

public class RrpWebRoutable implements RestletRoutable {

	@Override
	public Restlet getRestlet(Context context) {
		
		Router router = new Router(context);
		router.attach("/settings/ip", RrpResource.class);
		
		return router;
	        
	}

	@Override
	public String basePath() {
		
		 return "/rrp";
		 
	}

}
