package net.floodlightcontroller.rrp.restlet;

import java.util.List;

import net.floodlightcontroller.rrp.IRrpService;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class RrpResource extends ServerResource {

	@Get("json")
    public List<String> retrieve() {
		
        IRrpService rrp = (IRrpService)getContext().getAttributes().get(IRrpService.class.getCanonicalName());
        System.out.println("Retrieve Setting");
        
        return rrp.getPool();
        
    }
	
}
