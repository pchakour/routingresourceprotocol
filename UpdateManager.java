package net.floodlightcontroller.rrp;

import java.util.Map.Entry;

import org.openflow.util.HexString;

import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.packet.IPv4;


public class UpdateManager extends Thread{

	
	protected IProvider provider;
	protected RIB rib;
	protected Topology topology;
	protected Discover discover;
	
	
	public UpdateManager( IProvider provider ) {
		
		this.provider = provider;
		this.rib = provider.getRIB();
		this.topology = provider.getTopology();
		this.discover = provider.getDiscover();
		
	}
	
	
	/**
	 * 
	 */
	
	public void run() {
		
		for ( Entry<Long, IOFSwitch> s : provider.getFloodlightProvider().getAllSwitchMap().entrySet() )
			this.discover.start(s.getValue());
		
		IOFSwitch sw = null;
		Short outputPort = null;
		for ( Entry<Integer, Pair<String, Pair<IOFSwitch, Short>>> entry : this.topology.getAllHosts().entrySet() ) {
			
			sw = entry.getValue().getRight().getLeft();
			outputPort = entry.getValue().getRight().getRight();
			
			this.discover.scanPort( IPv4.toIPv4Address(Rrp.IP_CONTROLLER), entry.getKey(), 
					sw.getPort(outputPort).getHardwareAddress(), HexString.fromHexString(entry.getValue().getLeft()), sw, outputPort);
			
		}
		
	}
	
	
}
