package net.floodlightcontroller.rrp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.restserver.IRestApiService;
import net.floodlightcontroller.rrp.restlet.RrpWebRoutable;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;

import org.openflow.protocol.OFType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class Rrp implements  IFloodlightModule, IProvider, IRrpService {

	public final static short TCP_FLAG_SYN = 0x002;
	public final static short TCP_FLAG_SYN_ACK = 0x012;
	public final static short TCP_FLAG_RST = 0x004;
	
	public final static String IP_CONTROLLER = "10.0.20.254";
	public final static String IP_NETWORK_START = "10.0.20.1";
	public final static String IP_NETWORK_END = "10.0.20.255";
	
	protected ScheduledExecutorService executor;
	
	protected IFloodlightProviderService floodlightProvider;
	protected IRestApiService restApi;
	protected IStaticFlowEntryPusherService staticflowPusher;
	protected static Logger log = LoggerFactory.getLogger(Rrp.class);
	protected static MessageListener messageListener;
	protected static SwitchListener switchListener;
	protected static Topology topology;
	protected static RIB rib;
	protected static FIB fib;
	protected static Discover discover;
	protected static GarbageCollector garbageCollector;
	protected static UpdateManager updateManager;
	protected static FlowPusher flowPusher;
	protected static RoutingProcess routingProcess;
	
	
	
	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleServices() {
		 
		Collection<Class<? extends IFloodlightService>> l = new ArrayList<Class<? extends IFloodlightService>>();
	    l.add(IRrpService.class);
	    
	    return l;
		 
	}

	
	
	
	@Override
	public Map<Class<? extends IFloodlightService>, IFloodlightService> getServiceImpls() {
		
		Map<Class<? extends IFloodlightService>, IFloodlightService> m = new HashMap<Class<? extends IFloodlightService>, IFloodlightService>();
	    m.put(IRrpService.class, this);
	    
	    return m;
		
	}

	
	
	
	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleDependencies() {
		
		Collection<Class<? extends IFloodlightService>> l = new ArrayList<Class<? extends IFloodlightService>>();
		l.add(IFloodlightProviderService.class);
		l.add(IStaticFlowEntryPusherService.class);
		l.add(IRestApiService.class);
		
		return l;
		
	}

	
	
	
	
	@Override
	public void init( FloodlightModuleContext context ) throws FloodlightModuleException {
		
		this.floodlightProvider = context.getServiceImpl(IFloodlightProviderService.class);
		this.restApi = context.getServiceImpl(IRestApiService.class);
		this.staticflowPusher = context.getServiceImpl(IStaticFlowEntryPusherService.class);
		this.executor = Executors.newSingleThreadScheduledExecutor();
		
		topology = new Topology(this);
		rib = new RIB(this);
		fib = new FIB(this);
		discover = new Discover(this);
		messageListener = new MessageListener(this);
		switchListener = new SwitchListener(this);
		garbageCollector = new GarbageCollector(this);
		updateManager = new UpdateManager(this);
		flowPusher = new FlowPusher(this, this.staticflowPusher);
		routingProcess = new RoutingProcess(this);
		
		try {
		executor.scheduleAtFixedRate( garbageCollector, 30, 15, TimeUnit.SECONDS );
		executor.scheduleAtFixedRate( updateManager, 10, 10, TimeUnit.SECONDS);
		executor.scheduleAtFixedRate( routingProcess, 20, 15, TimeUnit.SECONDS);
		
		//Useful for debugging
		Runnable displayInfo = new Runnable() {
			
			@Override
			public void run() {
				
				System.out.println("------- TABLE -----------");
				System.out.println(rib);
				System.out.println("-------- TOPOLOGY ----------");
				System.out.println(topology);
				
			}
			
		};
		executor.scheduleAtFixedRate( displayInfo, 10, 10, TimeUnit.SECONDS );
		}catch(Exception e) { log.error(e.getMessage());}
	}
	
	
	
	
	
	@Override
	public void startUp(FloodlightModuleContext context) throws FloodlightModuleException {
		
		this.floodlightProvider.addOFSwitchListener(switchListener);
		this.floodlightProvider.addOFMessageListener(OFType.PACKET_IN, messageListener);
		this.restApi.addRestletRoutable(new RrpWebRoutable());
		
	}




	@Override
	public IFloodlightProviderService getFloodlightProvider() {
		
		return this.floodlightProvider;
		
	}




	@Override
	public Logger getLogger() {
		
		return log;
		
	}




	@Override
	public MessageListener getMessageListener() {
		
		return messageListener;
		
	}




	@Override
	public SwitchListener getSwitchLitener() {
		
		return switchListener;
		
	}




	@Override
	public Topology getTopology() {
		
		return topology;
		
	}




	@Override
	public RIB getRIB() {
		
		return rib;
		
	}


	@Override
	public FIB getFIB() {
		
		return fib;
		
	}

	@Override
	public Discover getDiscover() {
		
		return discover;
		
	}




	@Override
	public GarbageCollector getGarbageCollector() {
		
		return garbageCollector;
		
	}




	@Override
	public UpdateManager getUpdateManager() {
		
		return updateManager;
		
	}



	@Override
	public FlowPusher getFlowPusher() {
		
		return flowPusher;
		
	}
	
	
	@Override
	public RoutingProcess getRoutingProcess() {
		
		return routingProcess;
		
	}
	
	@Override
	public List<String> getPool() {
		
		List<String> l = new ArrayList<String>();
		l.add(IP_NETWORK_START);
		l.add(IP_NETWORK_END);
		l.add(IP_CONTROLLER);
		
		return l;
		
	}


	
	


}

